function validarFormulario() {
  var nombre = document.registro.nombre.value;
  var edad = document.registro.edad.value;
  if (nombre == "" || nombre.length < 3) {
    alert("Debe ingresar un nombre valido");
    return false;
  }
  var edadNumerica = parseInt(edad);
  if (isNaN(edadNumerica)) {
    alert("la edad no es un numero valido");
    return false;
  }
  if (edadNumerica < 18) {
    alert("se debe ser mayor de edad");
    return false;
  }
  return true;
}
