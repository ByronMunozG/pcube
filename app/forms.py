from django import forms
from .models import Contacto, Producto
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ValidationError

class ContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = '__all__'

class ProductoForm(forms.ModelForm):
    nombre = forms.CharField(min_length=3,max_length=50)
    imagen = forms.ImageField(required=True)
    precio = forms.IntegerField(min_value=1)

    def clean_nombre(self):
        nombre = self.cleaned_data["nombre"]
        existe = Producto.objects.filter(nombre__iexact=nombre).exists()
        if existe:
            raise ValidationError("Este producto ya está registrado")
        return nombre

    class Meta:
        model = Producto
        fields = '__all__'

        widgets = { 
        "fecha_fabricacion": forms.SelectDateWidget()
        }    

class CustomUserCreationForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ['username', "first_name", "last_name", "email", "password1", "password2"]

class ProductoEditForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        self._id = kwargs.pop('id',None)
        super().__init__(*args,**kwargs)

    nombre = forms.CharField(min_length=3,max_length=50)
    imagen = forms.ImageField(required=True)
    precio = forms.IntegerField(min_value=1)

    def clean_nombre(self):
        nombre = self.cleaned_data["nombre"]
        existe = Producto.objects.exclude(id=self._id).filter(nombre__iexact=nombre).exists()

        if existe:
            raise ValidationError("Este producto ya está registrado")
        return nombre

    class Meta:
        model = Producto
        fields = '__all__'

        widgets = { 
        "fecha_fabricacion": forms.SelectDateWidget()
        }  