from django.http import Http404
from django.contrib import messages
from django.shortcuts import render,get_object_or_404, redirect
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from .models import Producto
from .forms import ContactoForm, ProductoForm, CustomUserCreationForm, ProductoEditForm



# Create your views here.

def index(request):
    queryset = Producto.objects.all()

    data = {
        'object_list': queryset
    }
    return render(request,'app/index.html', data)

def products(request):
    queryset = Producto.objects.all()

    data = {
        'object_list': queryset
    }
    return render(request,'app/products.html', data)

def contact(request):
    data = {
        'form': ContactoForm()
    }

    if request.method =="POST":
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "contacto enviado"
        else:
            data["form"] = formulario

    return render(request,'app/contact.html',data)

def productdetail(request, id):
    obj = Producto.objects.get(id=id)
    context = {
        "object": obj
    }
    return render(request, 'app/product_detail.html', context)

@permission_required('app.add_producto')
def add_product(request):
    data = {
        'form': ProductoForm()
    }
    if request.method == "POST":
        formulario = ProductoForm(data=request.POST, files=request.FILES) 
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Producto agregado correctamente")
            return redirect(to="list-products")
        else:
            data["form"] =  formulario 
    return render(request,'app/product/add_product.html',data)
  
@permission_required('app.view_producto') 
def list_products(request):
    productos = Producto.objects.all()
    page = request.GET.get('page', 1)
    
    try:
        paginator = Paginator(productos , 7)
        productos = paginator.page(page)
    except:
        raise Http404

    data = {
        'entity': productos,
        'paginator': paginator
    }
    return render(request, 'app/product/to_list.html', data)

@permission_required('app.change_producto')
def modificar_producto(request, id):
    producto = get_object_or_404(Producto, id=id)
    data = { 
        'form': ProductoEditForm(instance=producto)
    }
    if request.method == "POST":
        formulario = ProductoEditForm(data=request.POST, instance=producto, files=request.FILES,id=id)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "Producto editado correctamente")
            return redirect(to="list-products")
        data["form"]=formulario
    return render(request, 'app/product/modify_product.html', data)

@permission_required('app.delete_producto')
def eliminar_producto(request, id):
    producto = get_object_or_404(Producto, id=id)
    producto.delete()
    messages.success(request, "Producto eliminado correctamente")
    return redirect(to="list-products")

def register(request):
    data = {
        'form': CustomUserCreationForm()
    }
    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"] )
            login(request, user)
            messages.success(request,"Usuario registrado correctamente")
            return redirect(to="index")
        data["form"] = formulario
    return render(request, 'registration/register.html', data)