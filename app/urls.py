from django.urls import path
from .views import index,products,contact,productdetail,add_product,list_products,modificar_producto,eliminar_producto, register

urlpatterns = [
    path('', index , name="index"),
    path('productos/', products, name="products"),
    path('sobre-nosotros/', contact, name="contact"),
    path('producto/<int:id>/', productdetail, name="product-detail"),
    path('agregar-producto/', add_product ,name="add-product"),
    path('listar-productos/', list_products, name="list-products"),
    path('modificar-producto/<int:id>/', modificar_producto, name="modify-product"),
    path('eliminar-producto/<id>/', eliminar_producto, name="delete-product"),
    path('registro/', register, name="register"),

]
